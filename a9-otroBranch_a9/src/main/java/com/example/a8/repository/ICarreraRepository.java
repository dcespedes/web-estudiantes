package com.example.a8.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.example.a8.modelo.Carrera;

public interface ICarreraRepository extends JpaRepository<Carrera, Integer> {

}
