package com.example.a8.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.a8.modelo.Carrera;
import com.example.a8.modelo.Usuario;
import com.example.a8.repository.ICarreraRepository;
import com.example.a8.repository.IUsuarioRepository;

@Controller
public class WebController {

	@Autowired
	private IUsuarioRepository rep;
	
	@Autowired
	private ICarreraRepository repCa;
	
	@ModelAttribute
	public Usuario setUsuario() {
		return new Usuario();
	}
	@ModelAttribute
	public Carrera setCarrera() {
		return new Carrera();
	}
		
	@GetMapping("/")
	public String inicio() {
		return "index";
	}
	
	@GetMapping("agregar")
	public String agregarUsuario(ModelMap model) {
		model.addAttribute("carreras", repCa.findAll());
		return "agregar";
	}
	@GetMapping("addCar")
	public String volver() {
		return "addCarreras";
	}
	
	@GetMapping("listar")
	public String mostrar(ModelMap model) {
		model.addAttribute("usuarios", rep.findAll());
		return "listar";
	}
	@PostMapping("agregarCar")
	public String addCarreras(@Valid @ModelAttribute("carrera") Carrera ca, BindingResult rs, ModelMap model) {
		if (rs.hasErrors()) {
			model.addAttribute("carreras", repCa.findAll());
			return "addCarreras";
			
		}else {
			repCa.save(ca);
			model.addAttribute("carreras", ca);
			model.addAttribute("carreras", repCa.findAll());
			return "agregar";
		}
		
	}
	@PostMapping("formulario")
	public String formulario(@Valid @ModelAttribute("usuario")  Usuario u, BindingResult rs,  ModelMap model) {
		if (rs.hasErrors()) {
			model.put("usuarios", rep.findAll());
			model.addAttribute("carreras", repCa.findAll());
			return "agregar";
		} else {
			rep.save(u);
			model.put("usuarios",u);
			model.put("usuarios", rep.findAll());
			return "listar";
		}
	}
	@PostMapping("modificar")
	public String modd(Usuario u, ModelMap model) {
		model.put("usuarios", rep.getOne(u.getRut()));
		model.put("carreras", repCa.findAll());
		return "modificar";
	}
	@PostMapping("modificarUsuario")
	public String modificar(@Valid @ModelAttribute("usuario") Usuario u, BindingResult rs, ModelMap model) {
		if (rs.hasErrors()) {
			model.put("usuarios", rep.findById(u.getRut()));
			model.put("carreras", repCa.findAll());
			return "modificar";
		} else {
			rep.save(u);
			model.put("usuarios", u);
			model.put("usuarios", rep.findAll());
			model.put("carreras", repCa.findAll());
			return "listar";

		}
	}
	
	@PostMapping("eliminar")
	public String eliminar(Usuario u, ModelMap model) {
		rep.deleteById(u.getRut());
		model.put("usuarios", rep.findAll());
		return "listar";
	}
	
	@PostMapping("buscarUsuario")
	public String buscarUsuario(String nombre, ModelMap model) 
	{
		model.addAttribute("usuarios", rep.findByNombre(nombre));
		return "listar";
	}
}
