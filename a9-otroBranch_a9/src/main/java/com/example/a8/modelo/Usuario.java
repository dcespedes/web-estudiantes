package com.example.a8.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
public class Usuario {

	@Id
	@Column(nullable = false)
	@Min(1)
	private int rut;

	@Column(name = "dv", nullable = false)
@Min(1)
	private int dv;

	@Column(length = 50, name = "nombre")
	@Size(min = 1, message = "Nombre no válido")
	private String nombre;

	@Column(length = 50, name = "apellido")
	@Size(min = 1, message = "Apellido no válido")
	private String apellido;

	@Column(name = "edad", nullable = false)
	@Min(18)
	@Max(70)
	private int edad;

	@ManyToOne
	private Carrera carrera;

	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public Usuario(int rut, int dv, @Size(min = 1, message = "Nombre no válido") String nombre,
			@Size(min = 1, message = "Apellido no válido") String apellido, int edad, Carrera carrera) {
		super();
		this.rut = rut;
		this.dv = dv;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.carrera = carrera;
	}

	public int getRut() {
		return rut;
	}

	public void setRut(int rut) {
		this.rut = rut;
	}

	public int getDv() {
		return dv;
	}

	public void setDv(int dv) {
		this.dv = dv;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	@Override
	public String toString() {
		return "Usuario [rut=" + rut + ", dv=" + dv + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad
				+ ", carrera=" + carrera + "]";
	}

}
