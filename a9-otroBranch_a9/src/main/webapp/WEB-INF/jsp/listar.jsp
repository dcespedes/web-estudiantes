<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../../../favicon.ico">

<title>Agregar Estudiantes</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link href="resources/css/estilo.css" rel="stylesheet">
<script src="resources/js/js.js"></script>
</head>

<body>

	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarTogglerDemo01"
				aria-controls="navbarTogglerDemo01" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
				<a class="navbar-brand" href="/"> <img alt="Logo"
					src="resources/img/Logo.png" style="width: 40px;"></a>
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="listar">Mostrar
							Estudiantes</a></li>.
					
				</ul>
				<form class="navbar-form navbar-right" action="buscarUsuario"
					method="post">
					<div class="form-group">
						<input type="text" class="form-control"
							placeholder="Buscar nombre estudiante" name="nombre">
					</div>
					<button value="buscar" type="submit" class="btn btn-default">Buscar</button>
				</form>
			</div>
		</nav>
	</header>


	<div class="jumbotron">
		<h1 class="display-3">Escuelita Grupo 1</h1>
		<h2>Lista de estudiantes</h2>
		<p class="lead">En esta p�gina podremos ver, eliminar y modificar
			a los estudiantes</p>

	</div>

	<br>
	<div class="table table-striped">
		<c:forEach items="${usuarios}" var="user">
			<table>
				<thead>
					<tr>
						<th>Run</th>
						<th>DV</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Edad</th>
						<th>Carrera</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>${user.rut}</td>
						<td>${user.dv}</td>
						<td>${user.nombre}</td>
						<td>${user.apellido}</td>
						<td>${user.edad}</td>
						<td>${user.carrera.nombre}</td>
						<td><form:form action="modificar" method="POST"
								modelAttribute="usuario">
								<form:hidden path="rut" value="${user.rut}" />
								<form:hidden path="dv" value="${user.dv}" />
								<form:button class="btn btn-success">Modificar</form:button>
							</form:form></td>
						<td><form:form action="eliminar" id="eliminar" method="POST"
								modelAttribute="usuario">
								<form:hidden path="rut" value="${user.rut}" />
								<form:hidden path="dv" value="${user.dv}" />
								<form:button onclick="eliminar(${user.rut})"
									class="btn btn-danger">Eliminar</form:button>
							</form:form>

							<form action="eliminar" method="post" id="del">
								<input type="hidden" name="rut" value="${user.rut}"> <input
									type="button" onclick="eliminar(${user.rut})" value="Eliminar">
							</form></td>
					</tr>
				</tbody>
			</table>
		</c:forEach>
	</div>
</body>
</html>