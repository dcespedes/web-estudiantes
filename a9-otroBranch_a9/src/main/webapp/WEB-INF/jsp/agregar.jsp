<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Agregar Estudiantes</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="resources/js/jquery-3.5.1.min.js"></script>
<script src="resources/js/js.js"></script>
<link href="resources/css/popUp.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarTogglerDemo01"
				aria-controls="navbarTogglerDemo01" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
				<a class="navbar-brand" href="/"> <img alt="Logo"
					src="resources/img/Logo.png" style="width: 40px;"></a>
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="listar">Mostrar
							Estudiantes</a></li>
				</ul>
				<form class="navbar-form navbar-right" action="buscarUsuario"
					method="post">
					<div class="form-group">
						<input type="text" class="form-control"
							placeholder="Buscar nombre estudiante" name="nombre">
					</div>
					<button value="buscar" type="submit" class="btn btn-default">Buscar</button>
				</form>
			</div>
		</nav>
	</header>



	<div class="jumbotron">
		<h1 class="display-3">Escuelita Grupo 1</h1>
		<h2>Agregar estudiantes</h2>
		<p class="lead">En esta p�gina podremos agregar los datos del
			estudiante</p>
		<button id="btn-abrir-popup" class="btn btn-lg btn-success btn-abrir-popup btn-success">Agregar
			Carreras</button>
			

	</div>

	<form:form action="formulario" method="POST" modelAttribute="usuario"
		class="form-horizontal">
		<div class="form-group">
			<form:label class="col-md-4 control-label" path="rut">Rut</form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarNum(event)"
					class="form-control input-md" type="text" path="rut"
					placeholder="Run" maxlength="8" />
				<form:errors cssClass="error" path="rut"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label class="col-md-4 control-label" path="dv">Dv </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarDV(event)" type="text"
					path="dv" placeholder="Digito Verificador"
					class="form-control input-md" maxlength="1" />

				<form:errors cssClass="error" path="dv"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label path="nombre" class="col-md-4 control-label">Nombre </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarLetra(event)" type="text"
					path="nombre" placeholder="Nombre" class="form-control input-md" />
				<form:errors cssClass="error" path="nombre"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label path="apellido" class="col-md-4 control-label">Apellido: </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarLetra(event)" type="text"
					path="apellido" placeholder="Apellido"
					class="form-control input-md" />
				<form:errors cssClass="error" path="apellido"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label path="edad" class="col-md-4 control-label">Edad: </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarNum(event)" type="number"
					path="edad" placeholder="Edad" class="form-control input-md" />

				<form:errors cssClass="error" path="edad"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label path="carrera" class="col-md-4 control-label">Escoge una carrera: </form:label>
			<div class="col-md-4">
				<form:select path="carrera">
					<form:options itemLabel="nombre" items="${carreras}" itemValue="id" />
				</form:select>

				<form:errors cssClass="error" path="carrera"></form:errors>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label" for="submit"></label>
			<div class="col-md-8">
				<form:button class="btn btn-success">Enviar</form:button>

			</div>
		</div>
	</form:form>

	<div class="popUp">
		<div class="col-md-8"></div>
	</div>


	<div class="overlay" id="overlay">
		<div class="popup" id="popup">
			<a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i
				class="fas fa-times"></i></a>
			<h3>Agregar Carrera</h3>
			<div class="contenedor-inputs">
				<form:form action="agregarCar" method="POST"
					modelAttribute="carrera" class="form-horizontal">
					<div class="form-group">
						<form:label path="nombre" class="col-md-4 control-label">Nombre </form:label>
						<div class="col-md-4">
							<form:input onkeypress="return validarLetra(event)" type="text"
								path="nombre" placeholder="Nombre de la carrera"
								class="form-control input-md" />
							<form:errors cssClass="error" path="nombre"></form:errors>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="submit"></label>
						<div class="col-md-8">
							<form:button class="btn btn-success">Enviar</form:button>

						</div>
					</div>
				</form:form>

			</div>
		</div>
	</div>
	<script src="resources/js/js.js"></script>
</body>
</html>