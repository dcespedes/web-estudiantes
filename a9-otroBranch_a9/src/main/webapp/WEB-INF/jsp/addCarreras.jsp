<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Agregar Carreras</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<script src="resources/js/jquery-3.5.1.min.js"></script>
<script src="resources/js/js.js"></script>
<link href="resources/css/estilo.css" rel="stylesheet">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarTogglerDemo01"
				aria-controls="navbarTogglerDemo01" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
				<a class="navbar-brand" href="/"> <img alt="Logo"
					src="resources/img/Logo.png" style="width: 40px;"></a>
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="listar">Mostrar
							Estudiantes</a></li>
					
				</ul>
				<form class="navbar-form navbar-right" action="buscarUsuario"
					method="post">
					<div class="form-group">
						<input type="text" class="form-control"
							placeholder="Buscar nombre estudiante" name="nombre">
					</div>
					<button value="buscar" type="submit" class="btn btn-default">Buscar</button>
				</form>
			</div>
		</nav>
	</header>
	<div class="jumbotron">
		<h1 class="display-3">Escuelita Grupo 1</h1>
		<h2>Agregar Carreras</h2>
		<p class="lead">En esta p�gina podremos agregar nuevas carreras</p>

	</div>

	<form:form action="agregarCar" method="POST" modelAttribute="carrera"
		class="form-horizontal">
		<div class="form-group">
			<form:label path="nombre" class="col-md-4 control-label">Nombre </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarLetra(event)" type="text"
					path="nombre" placeholder="Nombre de la carrera"
					class="form-control input-md" />
				<form:errors cssClass="error" path="nombre"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-4 control-label" for="submit"></label>
			<div class="col-md-8">
				<form:button class="btn btn-success">Enviar</form:button>

			</div>
		</div>
	</form:form>

</body>
</html>