<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modificar Estudiante</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="resources/js/js.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarTogglerDemo01"
				aria-controls="navbarTogglerDemo01" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
				<a class="navbar-brand" href="/"> <img alt="Logo"
					src="resources/img/Logo.png" style="width: 40px;"></a>
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="listar">Mostrar
							Estudiantes</a></li>
							
				</ul>
				<form class="navbar-form navbar-right" action="buscarUsuario"
					method="post">
					<div class="form-group">
						<input type="text" class="form-control"
							placeholder="Buscar nombre estudiante" name="nombre">
					</div>
					<button value="buscar" type="submit" class="btn btn-default">Buscar</button>
				</form>
			</div>
		</nav>
	</header>
	<div class="jumbotron">
		<h1 class="display-3">Escuelita Grupo 1</h1>
		<h2>Modificar Estudiante</h2>
		<p class="lead">En esta p�gina podremos modificar los datos del
			estudiante</p>

	</div>
	<form:form action="modificarUsuario" method="POST"
		modelAttribute="usuario" class="form-horizontal">
		<form:hidden path="rut" value="${usuarios.rut}" />
		<form:hidden path="dv" value="${usuarios.dv}" />
		<div class="form-group">
			<form:label path="nombre" class="col-md-4 control-label">Nombre </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarLetra(event)" type="text"
					path="nombre" class="form-control input-md"
					value="${usuarios.nombre}" />
				<form:errors class="errores"  path="nombre"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label path="apellido" class="col-md-4 control-label">Apellido: </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarLetra(event)" type="text"
					path="apellido" placeholder="Apellido"
					class="form-control input-md" value="${usuarios.apellido}" />
				<form:errors class="errores"  path="apellido"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label path="edad" class="col-md-4 control-label">Edad: </form:label>
			<div class="col-md-4">
				<form:input onkeypress="return validarNum(event)" type="number"
					path="edad" placeholder="Edad" class="form-control input-md"
					value="${usuarios.edad}" />

				<form:errors class="errores"  path="edad"></form:errors>
			</div>
		</div>
		<div class="form-group">
			<form:label path="carrera" class="col-md-4 control-label">Escoge una carrera: </form:label>
			<div class="col-md-4">

				<form:select path="carrera">
				<c:forEach items="${carreras}" var="car">
				<c:choose>
				<c:when test="${car.id eq usuarios.carrera.id}">
				<form:option value="${cl.id}" selected="true">${car.nombre}</form:option>
				</c:when>
				<c:otherwise>
				<form:option value="${car.id}">${car.nombre}</form:option>
				</c:otherwise>
				</c:choose>
				</c:forEach>
<%-- 					<form:option value="${usuarios.carrera.id}" --%>
<%-- 						label="${usuarios.carrera.nombre}"></form:option> --%>
<%-- 					<form:options itemLabel="nombre" items="${carreras}" itemValue="id" /> --%>
				</form:select>
				<form:errors class="errores"  path="carrera"></form:errors>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label" for="submit"></label>
			<div class="col-md-8">
				<form:button class="btn btn-success">Enviar</form:button>

			</div>
		</div>
	</form:form>

</body>
</html>