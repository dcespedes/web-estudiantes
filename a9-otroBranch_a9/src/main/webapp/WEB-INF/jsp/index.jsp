<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Inicio</title>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link href="resources/css/estilo.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarTogglerDemo01"
				aria-controls="navbarTogglerDemo01" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarTogglerDemo01">
				<a class="navbar-brand" href="/"> <img alt="Logo"
					src="resources/img/Logo.png" style="width: 40px;"></a>
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="listar">Mostrar
							Estudiantes</a></li>
					
				</ul>
				<form class="navbar-form navbar-right" action="buscarUsuario"
					method="post">
					<div class="form-group">
						<input type="text" class="form-control"
							placeholder="Buscar nombre estudiante" name="nombre">
					</div>
					<button value="buscar" type="submit" class="btn btn-default">Buscar</button>
				</form>
			</div>
		</nav>
	</header>


	<div class="jumbotron">
		<h1 class="display-3">Escuelita Grupo 1</h1>
		<p class="lead">Esta es la p�gina del Grupo 1, para agregar,
			buscar, modificar y eliminar estudiantes con Base de datos.</p>
		<p>
			<a class="btn btn-lg btn-success" href="agregar" role="button">Agregar
				Estudiante</a> <a class="btn btn-lg btn-success" href="addCar"
				role="button">Agregar Carrera</a>
		</p>
	</div>

	<div class="row marketing">
		<div class="col-lg-4">
			<img
				src="https://www.unav.edu/documents/26081803/26109168/curso-programacion.jpg"
				width="400px" height="250px">
		</div>

		<div class="col-lg-4">
			<div class="botonIntegrantes">
				<button id="btn-abrir-popup"
					class="btn-abrir-popup btn-outline-info">
					<img alt="" src="resources/img/Logo.png" width="30px" height="30px">Integrantes
				</button>
			</div>
		</div>
	</div>



	<footer class="footer">
		<p>� Company 2020</p>
	</footer>


	<div class="overlay" id="overlay">
		<div class="popup" id="popup">
			<a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i
				class="fas fa-times"></i></a>
			<h3>Integrantes</h3>
			<div class="contenedor-inputs">
				<p>Angela Romero</p>
				<p>Claudia Martinez</p>
				<p>Daniel C�spedes</p>
				<p>Nicolas Caro</p>
			</div>
		</div>
	</div>
	<script src="resources/js/js.js"></script>
</body>
</html>